function fizzBuzz(maxValue) {
    let string = ""
    for (let i = 1; i <= maxValue; i++){
        if(i%2 == 0) {
            string += "Fizz"
        }
        if(i%3 == 0) {
            string += "Buzz"
        }
        if(!(i%2 == 0 || i%3 == 0)) {
            string += i
        }
        string += ", "
    }
    console.log(string)
}
function fizzBuzzPrime(maxValue) {
    let string = ""
    for (let i = 1; i <= maxValue; i++){
        if(i%2 === 0) {
            string += "Fizz"
        }
        if(i%3 === 0) {
            string += "Buzz"
        }
        if(checkPrime(i)){
            string += "Prime"
        }
        if(!(i%2 == 0 || i%3 == 0 || checkPrime(i))) {
            string += i
        }
        string += ", "
    }
    console.log(string)
}
function checkPrime(value) {
    for(let i = 2; i < value; i++){
        if(value%i === 0){
            return false
        }
    }
    return true
}
fizzBuzz(20)
fizzBuzzPrime(20)